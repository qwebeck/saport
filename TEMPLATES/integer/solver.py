from saport.integer.model import Model


def run():
    model = Model("template model")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")

    model.add_constraint(x1 + x2 <= 6)
    model.add_constraint(5*x1 + 9*x2 <= 45)
    model.maximize(5 * x1 + 8 * x2)

    try:
        solution = model.solve()
        print(solution)
    except:
        raise AssertionError(
            "This problem has a solution and your algorithm hasn't found it!")


if __name__ == '__main__':
    run()
