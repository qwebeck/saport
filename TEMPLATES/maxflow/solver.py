from saport.maxflow.model import Network
from saport.maxflow.solvers.edmondskarp import EdmondsKarp
import os


def run():
    """
    Describe your problem in problem.txt
    """
    dirname = os.path.dirname(os.path.realpath(__file__))
    problem_path = "mx_problem.txt"

    net = Network.from_file(os.path.join(dirname, problem_path))
    maxflow = EdmondsKarp(net).solve()
    print(f'Maxflow is: {maxflow}')


if __name__ == '__main__':
    run()
