from saport.assignment.model import AssignmentProblem
from saport.assignment.hungarian_solver import Solver
import os


def run():
    # Describe your problem in file problem.txt
    # First line is 
    # <objective type> <n_workers> <n_tasks>
    dirname = os.path.dirname(os.path.realpath(__file__))
    problem_path = "a_problem.txt"
    model = AssignmentProblem.from_file(
        os.path.join(dirname, problem_path))
    solver = Solver(model)
    solution = solver.solve()
    print(solution)


if __name__ == '__main__':
    run()
