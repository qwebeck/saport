from saport.critical_path.solution import FullSolution
import saport.critical_path.solvers.simplex_solver_min as simplex_solver_min
import saport.critical_path.solvers.simplex_solver_max as simplex_solver_max
import saport.critical_path.solvers.cpm_solver as critical_path_solver
import os
import saport.critical_path.model as model


def run():
    dirname = os.path.dirname(os.path.realpath(__file__))
    # Define your problem in problem file
    #     - lines starting with # are comments
    #     - every other line corresponds to a single task and should be formatted as follows:
    #           <task name> <task duration> <task direct dependencies>
    #   whitespaces are significant!
    problem_path = "cp_problem.txt"
    dirname = os.path.dirname(os.path.realpath(__file__))
    project = model.Project.from_file(os.path.join(dirname, problem_path))

    # Każdy z solveró simpleksowe zwraca tylko optymalny wynik
    # Solver od networkx zwraca optymalny wynik oraz znalezioną ścieżkę jako listę list
    # (ponieważ w projekcie może być więcej niż jedna krytyczna ścieżka)
    # Prawdopodobnie to będzie ten solver który będzie używany
    # -----------------------------------
    # solver = simplex_solver_min.Solver(project)
    # solver = simplex_solver_max.Solver(project)
    solver = critical_path_solver.Solver(project)
    solution = solver.solve()
    #
    if isinstance(solution, FullSolution):
        for path in solution.critical_paths:
            print(" -> ".join(path))

    print("Minimal path duration: ", solution.duration)


if __name__ == '__main__':
    run()
