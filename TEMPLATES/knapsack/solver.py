from saport.knapsack.model import Problem
from saport.knapsack.solverfactory import SolverType, SolverFactory
import os


def run():
    """
    Describe problem in file kp_problem.txt
    n_items capacity
    value   weight
    value   weight
    .....
    """
    dirname = os.path.dirname(os.path.realpath(__file__))
    problem_path = "kp_problem.txt"
    problem = Problem.from_path(os.path.join(dirname, problem_path))
    timelimit = 10_000
    solver = SolverFactory.solver(
        SolverType.BRANCH_AND_BOUND_DFS, problem, timelimit)
    solution = solver.solve()
    print(solution)


if __name__ == "__main__":
    run()
