from __future__ import annotations
import saport.rankings.ahp as ahp
from dataclasses import dataclass
import numpy as np
from typing import Callable, Tuple, Type


def ranking_assignment():
    """
    Trójkątne macierze wag dla każdego z kryteriów.
    Zwykle są podane w poleceniu do zadania i powinno być ich tyle samo 
    ile parametrów mających wpływ na decyzję.
    Potrzeba przepisać tylko górną część macierzę. 
    Wiersza są odzielone przecinakami
    Resztę solver dopełni sam
    """
    Cs = (
        # Przykład tego jak wygląda pierwsza macierz po przeczytaniu
        #  1 1/7 1/5
        #  7  1   3
        #  5 1/3  1
        ahp.read_comparison_matrix("1/7,1/5;3"),
        ahp.read_comparison_matrix("5,9;4"),
        ahp.read_comparison_matrix("4,1/5;1/9"),
        ahp.read_comparison_matrix("9,4;1/4"),
        ahp.read_comparison_matrix("1,1;1"),
        ahp.read_comparison_matrix("6,4;1/3"),
        ahp.read_comparison_matrix("9,6;1/3"),
        ahp.read_comparison_matrix("1/2,1/2;1")
    )
    """
    Trójkątna macierz dla porówywania kryteriów między sobą.
    """
    c_p = ahp.read_comparison_matrix(
        "4,7,5,8,6,6,2;5,3,7,6,6,1/3;1/3,5,3,3,1/5;6,3,4,1/2;1/3,1/4,1/7;1/2,1/5;1/5")
    print("AHP")
    evm_solution = ahp.rate(ahp.evm, Cs, c_p)

    print("GMM")
    gmm_solution = ahp.rate(ahp.gmm, Cs, c_p)

    print(evm_solution)
    print(gmm_solution)


def consistency_assignment():
    """
    Liczy indeks saaty'ego i koczkodaja. dla podanej maicierzy
    """
    Cs = ahp.read_comparison_matrix("7,3;2")
    saaty_index = ahp.saaty_index(Cs)
    koczkodaj_index = ahp.koczkodaj_index(Cs)
    print(f'Saaty index: {saaty_index}')
    print(f'Koczkodaj index: {koczkodaj_index}')


if __name__ == '__main__':
    ranking_assignment()
    consistency_assignment()
