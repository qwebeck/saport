from saport.minimax.model import Game
from saport.minimax.solvers.pure import PureSolver
from saport.minimax.solvers.mixed import MixedSolver
import os


def run():
    """
    Describe your problem in problem.txt
    """
    dirname = os.path.dirname(os.path.realpath(__file__))
    problem_path = "mm_problem.txt"
    game = Game.from_file(os.path.join(dirname, problem_path))
    print(f"- original game: {game}")
    print("----------")

    pure_solver = PureSolver(game)
    pure_eqs = pure_solver.solve()
    print("- pure equilibriums:")
    if len(pure_eqs) == 0:
        print("There is no pure equilibrium in this game")
    else:
        print("===".join([f"{eq}" for eq in pure_eqs]))
    print("----------")

    mixed_solver = MixedSolver(game)
    mixed_eq = mixed_solver.solve()
    print("- mixed equilibrium:")
    print(mixed_eq)
    print("----------")


if __name__ == '__main__':
    run()
