from saport.simplex.model import Model


def run():
    model = Model("Example problem")
    # Describe your problem here
    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    model.add_constraint(2*x1 - x2 <= -1)
    model.add_constraint(x1 + x2 == 3)
    model.maximize(x1 + 3 * x2)

    try:
        solution = model.solve()
        print(solution)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    run()
