from unittest.runner import TextTestResult
import numpy as np
from numpy.core.numeric import count_nonzero
from .model import Assignment, AssignmentProblem, NormalizedAssignmentProblem
from typing import Any, Callable, List, Dict, Tuple, Set


class Solver:
    '''
    A hungarian solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    extract_mins(costs: np.Array):
        substracts from columns and rows in the matrix to create 0s in the matrix
    find_max_assignment(costs: np.Array) -> Dict[int,int]:
        finds the biggest possible assinments given 0s in the cost matrix
        result is a dictionary, where index is a worker index, value is the task index
    add_zero_by_crossing_out(costs: np.Array, partial_assignment: Dict[int,int])
        creates another zero(s) in the cost matrix by crossing out lines (rows/cols) with zeros in the cost matrix,
        then substracting/adding the smallest not crossed out value
    create_assignment(raw_assignment: Dict[int, int]) -> Assignment:
        creates an assignment instance based on the given dictionary assignment
    '''

    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        costs = np.array(self.problem.costs)

        while True:
            self.extracts_mins(costs)
            # Ciekawą rzeczą, na którą warto zwrócić uwagę,
            # jest to, że my nie zapamiętujemy przypisania,
            # a liczymy go od nowa po każdej minimalizacji kosztów w tabeli.
            # Generalnie, to jest potrzebne żeby obsłużyć taki przypadek kiedy jest jakiś pracownik,
            # który może wykonać dużo zadań małym kosztem i jakiś pracownik który może wykonać je dużym.
            # I może powstać taka sytuacja, że na początku my widzimy tylko tego dobrego pracownika,
            #  i przez przypadek przpisujemy mu to zadanie, które było najbardziej optymalne dla
            #  tego "slabszego" pracownika. I przez to, powodujemy nieoptymalny koszt.
            # Przykład:
            # 0  5  1
            # 0  1  6
            # 0  0  0
            #  ------- Pierwsze przypisanie
            # [0]  5   1
            #  0   1   6
            #  0  [0]  0
            # -------- Przekreślenie  i odejmowanie (przekreślamy pierwszą kolumnę i ostatni wiersz)
            # 0  4  0
            # 0  0  5
            # 0  0  0
            # -------- Drugie przypisanie
            # 0    4   [0]
            # 0   [0]   5
            # [0]  0    0
            # I to jest dobry wynik. Jeśli ktoś nie wierze, to można sprawdzić to na początkowej tabeli
            # i zobaczyć że rzeczywiści tak jest: koszt wynosi 2, i mniejszego po prostu nie da się osiągnąć.
            # A teraz zobaczmy co by było gdybyśmy, zapamętali to przypisanie co my zrobiliśmy w pierwszym kroku:
            # ---- Moment po przekreśleniu z zacznaczonym pierwszym przypisaniem:
            # [0]   4   0
            #  0    0   5
            #  0   [0]  0
            # ---- Drugie przypisanie
            # [0]   4   0
            #  0    0   [5] // odrazu wybieram 5, bo to jedyne pozostałe zadanie.
            #  0   [0]  0
            # Partzymy na orygianlną tabelę i widzimy, że sumaryczny koszt = 6
            # Więc nie jest to dobre podejście
            max_assignment = self.find_max_assignment(costs)

            if len(max_assignment) == self.problem.size():
                return self.create_assignment(max_assignment)
            self.add_zero_by_crossing_out(costs, max_assignment)

    def extracts_mins(self, costs):
        for i, row in enumerate(costs):
            costs[i] = row - min(row)
        for i, col in enumerate(costs.T):
            costs[:, i] = col - min(col)

    def add_zero_by_crossing_out(self, costs: np.array, partial_assignment: Dict[int, int]):
        """
        Najważniejsza część całego algorytmu.
        Algorytm polega na trzech prostych krokach:
            1. Pokryć wszystkie zera w tabeli minimalną liczbą linii
            2. Znaleźć najmniejszą nieprzekreśloną wartość i odjąć od wszystkich elementów w tabeli ( również od przekreślonych )
            3. Dodać do przerekślonych kolumn i wartość którą przed tym odjeliśmy
        """

        columns_to_cross = []
        not_assigned_rows = [i for i in range(
            costs.shape[0]) if i not in partial_assignment]
        row_that_shouldnt_be_crossed = [*not_assigned_rows]

        # Algorytm pokrycia wszystkich zer minimalną liczbą linii był dokładnie prezentowany na zajęciach
        # 1. Przechodzimy przez wszystkie wiersze dla których nie udało się przypisać wartości i
        #  dla każdego z nich:
        for row_idx in not_assigned_rows:
            # 1.2 Szukamy w kolumny które zawierają zera.
            row = costs[row_idx]
            columns_with_zeros = [i for i in range(
                costs.shape[0]) if row[i] == 0]
            # 1.3 Dodajemy do tych kolumn, które powinny być przekreślone
            columns_to_cross.extend(columns_with_zeros)
            # 1.4 Szukamy w kolumnie wiersz w którym było zrobione przypisanie do pracownika
            # W przypadku jeśli uda nam się znaleźć taki wiersz - dodajemy go do tych wierszy, co nie powinny być przekreślone
            for idx in columns_with_zeros:
                column_assignement = [
                    (row_idx, col_idx) for row_idx, col_idx in partial_assignment.items() if col_idx == idx]
                if column_assignement:
                    row_that_shouldnt_be_crossed.append(
                        column_assignement[0][0])

        # 2. Szukamy minimalnej wartości spośród przekreślonych wartości
        min_uncrossed = float('inf')
        for i in range(costs.shape[0]):
            for j in range(costs.shape[1]):
                if j not in columns_to_cross and i in row_that_shouldnt_be_crossed:
                    min_uncrossed = min(min_uncrossed, costs[i, j])
        # 3. Odejmujemy znalezioną wartość
        costs -= min_uncrossed
        # 4. Do każdego przekreślonego wiersza i kolumny dodajemy wcześniej odjętą wartość
        for j in columns_to_cross:
            costs[:, j] += min_uncrossed
        for i in range(costs.shape[0]):
            if i not in row_that_shouldnt_be_crossed:
                costs[i] += min_uncrossed

    def find_max_assignment(self, costs: np.array) -> Dict[int, int]:
        """
        Ta funkcja wykonuje jedną z najważniejszych części algorytmu węgierksiego, a mianowicie, 
        przypisuje zadania do pracowników. 
        Zasada działania funckji jest dość prosta:
        1. Najpierw ona szuka wszystkich pracowników, które mogą wykonać zadania zerowym kosztem
        2. Póki ona może przypisywać zadania, ona przechodzi przez wszystkich znalezionych pracowników i 
        szuka dla nich takie zadania które oni mogą wykonać niajmniejszym kosztem
        3. W następnym kroku, my wybiremy spośród tych zadań takiem które mogą w jakimś stopniu pomóc z pracą nad pracownikami      
        """
        can_assign = True
        assignements = {}

        # Słowniki zawierają pracowników, które mają jakieś 0 w kolumni
        available_workers = self._create_idx_to_zero_count_dict(costs)
        unassigned_tasks = self._create_idx_to_zero_count_dict(costs.T)

        while can_assign:
            # Najpierw szukamy takiego pracownika, który posiada najmniej dostępnych zadań
            least_optimal_worker_idx = self._find_least_value_key(
                available_workers)
            # W kolejnym kroku, szukamy zadania które ten pracownik może wykonać zerowym kosztem.
            # Jest to nam potrzebne, żeby przypisać do niego najbardziej optymalne zadanie
            worker_tasks_with_zero_cost = [task_idx for task_idx, task_cost in enumerate(
                costs[least_optimal_worker_idx]) if task_cost == 0]

            # W poniższych liniach, ze wszystkich dostępnych zadań, my najpierw wybieramy takie, które
            # mogą być wykonane przez pracownika (robimy to poprzez przekazywanie funkcji is_appliable_to_worker do funkcji
            # _find_least_value_key), a potem spośród tych zadań my wybieramy takie,
            # Mogą być wykonane przez najmniejszą liczbę pracowników.
            # Znalezione zadanie, my przypisujemy do zmiennej best_fit_for_worker_idx
            def is_appliable_to_worker(task_idx, _):
                return task_idx in worker_tasks_with_zero_cost
            best_fit_for_worker_idx = self._find_least_value_key(
                unassigned_tasks, is_appliable_to_worker)
            # Usuwamy znaleznionego pracownika dlatego, nawet w przypadku jeśli nie udało się znaleźć rozwiązanie,
            # żeby nie powtarzać przeszukiwania jeszcze raz
            del available_workers[least_optimal_worker_idx]
            if best_fit_for_worker_idx is None:
                continue
            del unassigned_tasks[best_fit_for_worker_idx]
            assignements[least_optimal_worker_idx] = best_fit_for_worker_idx
            # Przestajemy iterować w przypadku jeśli nie mamy więcej dostępnych pracowników albo w
            # przypadku jeśli nie mamy dostępnych zadań
            if len(available_workers.keys()) == 0 or len(unassigned_tasks.keys()) == 0:
                can_assign = False
        return assignements

    def _find_least_value_key(self, dictionary: Dict[Any, int], predicate=lambda key, value: True) -> int:
        """
        Utility function

        In dictionary finds key, that has smallest value assigned
        Example:
        {
            key_1: 2,
            key_2: 0
        }
        least_key = key_2
        """
        sequence = [item for item in dictionary.items()
                    if predicate(item[0], item[1])]
        if not sequence:
            return None
        _, key = min((val, key) for key, val in sequence)
        return key

    def _create_idx_to_zero_count_dict(self, costs):
        """
        Utility funciton 

        Creates map, that maps row index to count of zeros in this row.
        Not adds a key if there are no zeros in row
        Example for matrix:
            1 1 1
            1 0 0
            1 0 1 
        dicticitonary = {
            1: 2,
            2: 1
        }
        """
        dictionary = {}
        for row_idx, row in enumerate(costs):
            zero_count = len(row) - np.count_nonzero(row)
            if zero_count == 0:
                continue
            dictionary[row_idx] = zero_count
        return dictionary

    def create_assignment(self, raw_assignment: Dict[int, int]) -> Assignment:
        assignnments = [-1 for i in range(
            self.problem.original_problem.costs.shape[0])]
        objective = 0
        for worker, task in raw_assignment.items():
            x, y = self.problem.original_problem.costs.shape
            if worker >= x or task >= y:
                continue
            cost = self.problem.original_problem.costs[worker, task]
            assignnments[worker] = task
            objective += cost
        return Assignment(assignnments, objective)
