import numpy as np
from .model import AssignmentProblem, Assignment, NormalizedAssignmentProblem
from ..simplex.model import Model
from ..simplex.expressions.expression import Expression
from dataclasses import dataclass
from typing import List
from collections import defaultdict

from saport.simplex.expressions import expression


class Solver:
    '''
    A simplex solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    '''

    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        model = Model("assignment")
        variables = defaultdict(list)
        row_c, col_c = self.problem.costs.shape
        objective = Expression()

        for i in range(row_c):
            for j in range(col_c):
                # Dla każdego kosztu, który istniej tworzymy zmienną, którą potem dodamy do modelu
                var = model.create_variable(f"c{i}{j}")
                # Wartość każdej zmiennej może równać się 0 lub 1, co odpowiada statusu przypisane/nieprzypisane zadanie
                model.add_constraint(var <= 1)
                term = self.problem.costs[i, j] * var
                objective += term
                # Tworzę kolumny i wiersze, żeby potem łatwiej było nakładać ograniczenia na to, że suma zmiennych
                # w jednym wierszu i w jednej kolumnie powinna być równa 1.
                variables[f"row_{i}"].append(var)
                variables[f"col_{j}"].append(var)

        model.minimize(objective)

        # Dodawanie ograniczeń na sumę wierszy i kolumn
        # Ograniczenie wynika z tego, że jeden pracownik może wykonywać jedno zadanie,
        # i jedno zadanie może być wykonywane tylko przez jednego pracownika
        for i in range(row_c):
            expression = sum(variables[f"row_{i}"], Expression())
            model.add_constraint(expression == 1)
        for j in range(col_c):
            expression = sum(variables[f"col_{j}"], Expression())
            model.add_constraint(expression == 1)

        solution = model.solve()

        # Assigment utworone w taki sposób, że indeks odpowiada pracowniku,
        # a wartość od tym indeksem - przypisanemu zadaniu.
        # Na początku wypełniamy to wszystko -1, ponieważ może być tak, że liczba pracowników
        # w problemie początkowym była różna od ilości zadań i z racji tego my dokladaliśmy "sztucznych" pracowników,
        # albo "stuczne" zadania.
        # Założeniem testów jest to, że  taki solver powinien zwrócić -1 dla takich przypadków
        assigment = [-1 for i in range(self.problem.costs.shape[0])]

        for i in range(self.problem.original_problem.costs.shape[0]):
            row_values = [solution.value(v) for v in variables[f'row_{i}']]
            task = row_values.index(1)
            if task < self.problem.original_problem.costs.shape[1]:
                assigment[i] = task

        objective = sum([self.problem.original_problem.costs[w, t] for w, t in enumerate(
            assigment) if t >= 0])

        return Assignment(assigment, objective)
