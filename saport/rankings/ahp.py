from __future__ import annotations
from dataclasses import dataclass
from networkx.generators import geometric
import numpy as np
import networkx as nx
import sys
from typing import Callable, List, Tuple


@dataclass
class RankingSolution:
    rankings: Tuple[np.Array]
    preference_ranking: np.Array
    global_ranking: np.Array
    choice: int

    def __str__(self) -> str:
        result = '-' * 20
        result += "Rankings:\n"
        for ranking in self.rankings:
            s = ["{:.3f}".format(v) for v in ranking]
            s = " | ".join(map(str, s))
            result += f"{s}\n"

        result += "Preference rankings:\n"
        for ranking in self.rankings:
            s = ["{:.3f}".format(v) for v in ranking]
            s = " | ".join(map(str, s))
            result += f"{s}\n"

        s = ["{:.3f}".format(v) for v in self.preference_ranking]
        result += f"Global ranking: {' ,'.join(s)}\n"
        result += f"Choice: {self.choice}\n"
        result += '-' * 20
        result += "\n"
        return result


@dataclass
class ConsistencySolution:
    saaty: float
    koczkodaj: float


def rate(ranking_method: Callable[[np.array], np.array], pairwise_comparisons: np.array,  preferences: np.array):
    """
    Wybiera najlepszą opcję z dostępnych porównań na podstawie przypisanego kosztu

    :param ranking_method: metoda rankingowa.  evm, gmm
    :param pairwise_comparisons: lista zawierająca macierze porównań każdy z każdym dla każdej opcji według każdego kryterium. 
    :param preferences: macierz zawierająca nasze eksperckie preferencje na podstawie których porównujemy kryteria.
    Trochę dokładniej z tym o co chodzi. 
    Generalnie mamy do czynienia z takim problemem: 
        U nas jest trzy domy i my chcemy ocenić który z tych domów jest lepszy.
        Żeby długo się nad tym nie zastanwiać i nie wymyślać niewiadomo co, podchodzimy do tego jako mądrze ludzie:
        siadamy i tworzymy macierz preferencji w której pokazujemy na ile jeden kryterium jest ważniejszy od innego.
        Coś w stylu:
                        cena     |        odległość 
                                 |       od centrum
        cena             1       |         5
        --------------------------------------------------
        odległość     1/5        |         1
        od centrum               |

        I to oznacza, że odległośc od centrum jest dla nas o wiele ważniejsza niż cena
        Tak powstaje parameter `preferences`. 


        Kolejna rzecz, którą robimy, to bierzermy wszystkie domy które my mamy i porównujemy je wzgłędem każdego z kryteriów.
        Czyli, najpeirw porównujemy cenę pierwszego domu z pierwszym, potem drugiego z pierwszym i tak dla każdego z kryteriów 
        powstaje coś w stylu:
                        CENA
                    dom    1     |        dom 2 
                                 |       
        dom 1            1       |         1/2
        --------------------------------------------------
        dom 2         2          |         1

        I to oznacza, że naszym zdaniem dom 1 ma lepszą cenę niż dom 2.
        Tak powstaje pairwise_comparisons.


        Okej. 
        Teraz algorytm.
        1. Tworzymy ranking według każdego z kryteriów. (wzgłędem ceny ranking mógłby wyglądać tak: [dom 1, dom 2] )
        2. Tworzymy ranking kryteriów (np. [odległośc od centrum, cena])
        3. Tworzymy globalny ranking. Czyli dla każdego z domów, mnozymy jego wagę w lokalnym rankingu razy wagę kryterium dla 
        którego ranking był ułożony
        4. Wybieramy dom, który jest najwyżej w globalnym rankingu
    """
    local_rankings = [ranking_method(matrix)
                      for matrix in pairwise_comparisons]
    cryteria_weights = ranking_method(preferences)
    global_ranking = []
    num_choices = len(local_rankings[0])
    for option in range(num_choices):
        option_weights = np.array([local_ranking[option]
                                   for local_ranking in local_rankings])
        option_score = option_weights @ cryteria_weights
        global_ranking.append(option_score)

    choice = np.where(np.array(global_ranking) ==
                      max(global_ranking))[0][0]
    return RankingSolution(
        local_rankings, cryteria_weights, global_ranking, choice)


def read_float(number: str) -> float:
    '''
    Just a helper function to read rational numbers (fractions) into floats, i.e. "1/5" -> 0.2
    '''
    try:
        return float(number)
    except ValueError:
        num, denom = number.split('/')
        return float(num) / float(denom)


def read_upper_traingular_row(row: List[str]) -> List[float]:
    return [read_float(n) for n in row.split(",")]


def read_upper_triangular(s: str) -> List[List[float]]:
    return [read_upper_traingular_row(r) for r in s.split(";")]


def fill_comparison_matrix_from_upper_triangular(upper_triangular: List[List[float]]) -> np.array:
    '''
        Creates a comparison matrix based on the upper triangular part of the matrix, i.e.
        an upper triangular matrix:

        0 2 3
        0 0 4
        0 0 0

        we should get a full comparison matrix:

        1    2    3
        1/2  1    4
        1/3  1/4  1
    '''
    size = len(upper_triangular) + 1
    comparison_matrix = np.eye(size, dtype=float)
    for row in range(size - 1):
        comparison_matrix[row, 1+row:] = upper_triangular[row]
    for row in range(1, size):
        for col in range(0, row):
            comparison_matrix[row, col] = 1 / comparison_matrix[col, row]

    return comparison_matrix


def read_comparison_matrix(s: str) -> np.array:
    '''
    Just a helper function to read a comparison matrix from upper triangular matrix
    written in a simple format, where "," separates cols and ";" rows, e.g.

    "2,3;4" = [[2,3],[4]] 

    which represents: 

    0 2 3
    0 0 4
    0 0 0 
    '''
    upper_triangular_matrix = read_upper_triangular(s)
    return fill_comparison_matrix_from_upper_triangular(upper_triangular_matrix)


def principal_eigenvector(c: np.Array) -> List[float]:
    graph = nx.from_numpy_matrix(c.T, create_using=nx.DiGraph)
    return np.array([v for v in nx.eigenvector_centrality_numpy(graph, weight='weight').values()])


def principal_eigenvalue(c: np.Array) -> float:
    pe = principal_eigenvector(c)
    nz_index = pe.nonzero()[0][0]
    return c.dot(pe)[nz_index]/pe[nz_index]


def normalize_vector(w: np.Array) -> np.Array:
    return w / sum(w)


def evm(c: np.array) -> np.Array:
    return normalize_vector(principal_eigenvector(c))


def gmm(c: np.Array) -> np.Array:
    rates = [gmean(row) for row in c]
    return normalize_vector(rates)


def gmean(w: np.array):
    return w.prod()**(1.0/len(w))


def triads(c: np.array):
    """
    Zwraca generator tworzcy triady spełniające warunek
    1 <= i < j < k
    """
    n, _ = c.shape
    return ((i, j, k) for i in range(n) for j in range(i+1, n) for k in range(j+1, n))


def saaty_index(c: np.Array) -> float:
    lambda_max = principal_eigenvalue(c)
    number_of_alternatives, _ = c.shape
    return (lambda_max - number_of_alternatives) / (number_of_alternatives - 1)


def koczkodaj_index(c: np.Array) -> float:
    local_indexes = []
    for i, j, k in triads(c):
        p1 = abs(1 - c[i, j]/(c[i, k] * c[k, j]))
        p2 = abs(1 - (c[i, k] * c[k, j])/c[i, j])
        local_indexes.append(min(p1, p2))
    global_index = max(local_indexes)
    return global_index


if __name__ == "__main__":
    '''
    Reads the upper triangular from the first arg and calculates normalized principal eigenvector
    '''
    comparison_matrix_string = sys.argv[1]
    comparison_matrix = read_comparison_matrix(comparison_matrix_string)
    print(evm(comparison_matrix))
