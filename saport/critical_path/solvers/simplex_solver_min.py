from ..model import Project
from ..project_network import ProjectNetwork
from ...simplex.model import Model
from ...simplex.expressions.expression import Expression
from ..solution import BasicSolution


class Solver:
    '''
    Simplex based solver looking for the critical path in the project.
    Uses linear model minimizing total duration of the project.

    Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project
    model: simplex.model.Model
        a linear model looking for the quickest way to finish the project
    Methods:
    --------
    __init__(problem: Project)
        create a solver for the given project
    create_simplex_model() -> simplex.model.Model
        builds a linear model of the problem
    solve() -> BasicSolution
        finds the duration of the critical (longest) path in the project network
    '''

    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)
        self.model = self.create_model()

    def create_model(self) -> Model:
        """
        To moim zdaniem jest jeden z najprostszych algorytmów które my mieliśmy:
        W tym solwerze, naszym zadaniem jest znaleźć minimalny 
        czas w którym może być wykonany projekt.
        Tłumacząc na język LP:
        tworzymy model liniowy mający za cel zminimalizować czas który będzie stracony między początkową 
        i końcową czynnościami naszego projektu.
        W tym celu, my:
            1. Dla każdej czynności która istniej w modelu tworzymy zmienną opisującą ile czasu potrzebujemy
            żeby dojść do tego etapu 
            2. Ustawiamy ograniczenie na to, że różnica między każdą z czynnością powinna być
            większa lub równa od czasu potrzebnego na to, żeby przejść między dwoma tymi wierzchołkami
            3. Ustawiamy funkcję minimalizującą w taki sposób, żeby ona zminimalizowała czas przejścia 
            między wierzchołkiem początkowym i końcowym
        """
        model = Model("critical path (min)")
        nodes_to_vars = {}
        for node in self.project_network.nodes():
            # node - ma typ ProjectState, który jest zdefiniowany jako dataclass jednym z pół którego jest index
            var = model.create_variable(f"t_{node.index}")
            nodes_to_vars[node.index] = var

        for start, end, _ in self.project_network.edges():
            time = self.project_network.arc_duration(start, end)
            start_var = nodes_to_vars[start.index]
            end_var = nodes_to_vars[end.index]
            model.add_constraint(start_var - end_var >= time)

        goal = nodes_to_vars[self.project_network.goal_node.index]
        start = nodes_to_vars[self.project_network.start_node.index]
        model.minimize(start - goal)
        return model

    def solve(self) -> BasicSolution:
        solution = self.model.solve()
        return BasicSolution(int(solution.objective_value()))
