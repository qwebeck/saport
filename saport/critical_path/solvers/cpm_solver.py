from ..solution import FullSolution
from typing import List, Dict
from ..project_network import ProjectState, ProjectNetwork
from ..model import Project
from networkx.algorithms.shortest_paths.unweighted import predecessor
import networkx as nx
from os import name
from copy import copy


class Solver:
    '''
    A "critical path method" solver for the given project.

        Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project

    Methods:
    --------
    __init__(problem: Project):
        create a solver for the given project
    solve -> FullSolution:
        solves the problem and returns the full solution
    forward_propagation() -> Dict[ProjectState,int]:
        calculates the earliest times the given events (project states) can occur
        returns a dictionary mapping network nodes to the timestamps
    backward_propagation(earliest_times: Dict[ProjectState, int]) -> Dict[ProjectState,int]:
        calculates the latest times the given events (project states) can occur
        uses earliest times to start the computation
        returns a dictionary mapping network nodes to the timestamps
    calculate_slacks(earliest_times: Dict[ProjectState, int], latest_times: Dict[ProjectState,int]) -> Dict[str, int]:
        calculates slacks for every task in the project
        uses earliest times and latest time of the events in the computations
        returns a dictionary mapping tasks names to their slacks
    create_critical_paths(slacks: Dict[str,int]) -> List[List[str]]:
        finds all the critical paths in the project based on the tasks' slacks
        returns list containing paths, every path is a list of tasks names put in the order they occur in the critical path 
    '''

    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)

    def solve(self) -> FullSolution:
        earliest_times = self.forward_propagation()
        latest_times = self.backward_propagation(earliest_times)
        duration = earliest_times[self.project_network.goal_node]
        task_slacks = self.calculate_slacks(earliest_times, latest_times)
        critical_paths = self.create_critical_paths(task_slacks)
        return FullSolution(duration, critical_paths, task_slacks)

    def forward_propagation(self) -> Dict[ProjectState, int]:
        """
        W tym kroku, naszym zadaniem jest przypisać do każdego wierzchołka który istnieje 
        w naszym grafie maksmylany czas dojścia do niego. 
        Algorytm jest dość prosty:
            1. Dla każdego wierzhcołka (W) w grafie ( oprócz początkowego ):
                1. Dla każdego wierzchołka (WW) który jest połączony współną krawędzią z (W):
                    1. Oblicz koszt dojścia od tego (WW) do (W) jako sumę dojścia od źródła do (W) + koszt krawędzi
                Przypisz maksymalną wartość kosztu jako koszt dojścia do (W) 
        """
        """
        earliest_times - słownik który zawiera wpisy w postaci 
            node (czynność): minimalny czas potrzebny na to, żeby ta czynność się wydarzyła

        Pewnie w tym momencie coś ci się nie zgadza: w algorytmie wyżej jest napisane, że zapisujemy do słownika maksymalną wartość 
        z wszystkich wartości wchodzących, a teraz powiedziałem, że słownik zawiera wartości minimalne. 
        Żeby to zrozumieć, potrzeba przypmonieć sobie to, w jaki sposób my konstruowaliśmy naszą sieć:
            * węzęł - to jakiś stan systemu ( inaczej czynność )
            * krawędzie - to zadania które powinny być wykonane, żeby projekt zmienił swój stan ( zadania wykonują się równoległe )
        Czyli, czas potrzebny na to, żeby system przeszedł w ten stan -- to czas potrzebny na wykonanie najdłuższego zadania
        Mam nadzieję, że teraz jest lepiej :D
        """
        earliest_times = {
            self.project_network.start_node: 0
        }
        for node in self.project_network.nodes():
            if node == self.project_network.start_node:
                continue
            # węzły połączone współną krawędzią
            p = list(self.project_network.predecessors(node))
            task_costs = [self.project_network.arc_duration(
                pred_node, node) + earliest_times[pred_node] for pred_node in p]
            earliest_times[node] = max(task_costs)
        return earliest_times

    def backward_propagation(self, earliest_times: Dict[ProjectState, int]) -> Dict[ProjectState, int]:
        """
        W tym kroku, naszym zadaniem jest przypisać do każdego wierzchołka który istnieje 
        w naszym grafie maksmylany czas dojścia do niego. 
        Algorytm jest dość prosty:
            1. Dla każdego wierzhcołka (W) w grafie ( oprócz początkowego ):
                1. Dla każdego wierzchołka (WW) który jest połączony współną krawędzią z (W):
                    1. Oblicz koszt dojścia (WW) do (W) jako różnicę czasu potrzebnego na dojście do poprzedniego zadania i czasu który odpowiada krawędzi WW_W
                Przypisz maksymalną wartość kosztu jako koszt dojścia do (W) 
        """
        goal_node = self.project_network.goal_node
        latest_times = {
            # My wymagamy, żeby najpóźniejszy możliwy czas dojścia do naszego wyniku
            # był minimalnym możliwym czasem dojścia do niego
            goal_node: earliest_times[goal_node]
        }
        # [::-1] - potrzebuję to, żeby przejść przez listę wierzchołków od końca
        for node in self.project_network.nodes()[::-1]:
            if node == self.project_network.goal_node:
                continue

            # węzły połączone współną krawędzią
            p = self.project_network.successors(node)
            task_cost = [latest_times[succ_node] -
                         self.project_network.arc_duration(node, succ_node) for succ_node in p]
            latest_times[node] = min(task_cost)
        return latest_times

    def calculate_slacks(self, earliest_times: Dict[ProjectState, int], latest_times: Dict[ProjectState, int]) -> Dict[str, int]:
        # slack of the task equals "the latest time of its end" minus "earliest time of its start" minus its duration
        # tip: remember to ignore dummy tasks (task.is_dummy could be helpful)
        slacks = {}
        for start, end, _ in self.project_network.edges():
            task = self.project_network.arc_task(start, end)
            if task.is_dummy:
                continue
            slacks[task.name] = latest_times[end] - \
                earliest_times[start] - task.duration
        return slacks

    def create_critical_paths(self, slacks: Dict[str, int]) -> List[List[str]]:
        """
        W poprzednich krokach my znaleźliśmy to, ile czasu zajmuje każda z akcji, w ścieżce krytycznej, ale my 
        nie wiemy tego jakie to są akcje. 
        Więc w tym kroku zajmujemy się tym, że szukamy odpowiednie akcje
        """
        network = copy(self.project_network)
        for start, end, _ in network.edges():
            task = self.project_network.arc_task(start, end)
            if task.is_dummy:
                continue
            if slacks[task.name] != 0:
                network.remove_edge(start, end)

        result = []
        pathes = list(nx.all_simple_paths(
            network.network, network.start_node, network.goal_node))

        for path in pathes:
            path = zip(path, path[1:])
            tasks = [network.arc_task(start, end)
                     for start, end in path]
            tasks = [task.name for task in tasks if not task.is_dummy]
            result.append(tasks)
        return result
