from collections import defaultdict
from saport.simplex.expressions import objective
from ..model import Project
from ..project_network import ProjectNetwork
from ...simplex.model import Model
from ...simplex.expressions.expression import Expression
from ..solution import BasicSolution


class Solver:
    '''
    Simplex based solver looking for the critical path in the project.
    Uses linear model maximizing length of the path in the project network. 

    Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project
    model: simplex.model.Model
        a linear model looking for the maximal path in the project network
    Methods:
    --------
    __init__(problem: Project)
        create a solver for the given project
    create_simplex_model() -> simplex.model.Model
        builds a linear model of the problem
    solve() -> BasicSolution
        finds the duration of the critical (longest) path in the project network
    '''

    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)
        self.model = self.create_simplex_model()

    def create_simplex_model(self) -> Model:
        """
        Ten model też jest w miarę prosty, ale może być ciut nieintuicyjny. 
        Ale myślę, że sobie poradzimy :D
        Cała idea algorytmu sprowadza się do tego, że kiedyś ktoś mądry zauważył, że minimalny czas 
        trwania projektu - to czas potrzebny na to, żeby wykonać te zadania które zajmują najwęcej czasu 
        w całym projekcie. ( przy założeniu, że wszyskit procesy wykonują się równoległe )
        Tłumacząc na simpleksowy:
        my powinniśmy znaleźć maksymalną ścieżkę w naszym grafie. 
        Więc po kolei:
            1. dla każdej krawędzi która istnieje w naszym modelu tworzymy zmienną która przyjmuje 
            wartości ze zbioru {0, 1} i wskazuje na to czy my przechodzimy tą ścieżką czy nie.
            2. dla wszystkich zmiennych które wychodzą z wierzchołka startowego, tworzymy ograniczenie na to, 
            że ich suma powinna wynieść 1 (my szukamy JEDNĄ maksymalną ścieżkę, więc 
            ona może wychodzić ze źródła tylko poprzez jedną ścieżkę). 
            Robimy analogicznie dla wierzchołka końcowego
            3. Dla każdego wierzchołka my tworzymy ograniczenie na to, że suma krawędzi wchodzących
             powinna równać się sumie krawędzi wychodzącyh
            4. Jako funkcję celu ustawiamy funkcję maksymulizującą długość ścieżki. Długość wyznaczmy jako
            czas potrzebny na to, żeby nią przejć 
        """
        model = Model("critical path (max)")
        node_to_expression = defaultdict(Expression)
        objective = Expression()
        # 0) we need as many variables as there is edges in the project network
        for i, edge in enumerate(self.project_network.edges()):
            x = model.create_variable(f"x_{i}")
            # prejście albo istnieje albo nie istniej
            # 1) every variable has to be <= 1
            model.add_constraint(x <= 1)
            start, end, _ = edge
            node_to_expression[start] -= x
            node_to_expression[end] += x
            objective += x * self.project_network.arc_duration(start, end)

        # 2) sum of the variables starting at the initial state has to be equal 1
        from_start_sum = -1 * \
            node_to_expression[self.project_network.start_node]
        model.add_constraint(from_start_sum == 1)
        # 3) sum of the variables ending at the goal state has to be equal 1
        to_goal_sum = node_to_expression[self.project_network.goal_node]
        model.add_constraint(to_goal_sum == 1)
        # 4) for every other node, total flow going trough it has to be equal 0
        #    i.e. sum of incoming arcs minus sum of the outgoing arcs = 0
        for node in self.project_network.normal_nodes():
            expression = node_to_expression[node]
            # Ścieżka nie może skończyć się w środku sieci, więc dodajemy ograniczenie na sumę wchodzących i wychodzących
            model.add_constraint(expression == 0)
        # 5) we have to maximize length of the path
        #    (sum of variables weighted by the durations of the corresponding tasks)
        model.maximize(objective)
        return model

    def solve(self) -> BasicSolution:
        solution = self.model.solve()
        return BasicSolution(int(solution.objective_value()))
