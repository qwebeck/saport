# Setup
Zanim skorzystać z saporta, potrzeba skolonować sobie repozytorium, potem uruchomić pip install -e . z dyrektorii do której on będzie sklonowany.
```
git clone https://gitlab.com/qwebeck/saport.git
cd saport
pip install -e .
```
To polecenie zainstaluje wsystkie potrzebne zależności oraz zrobi tak, żeby python widział modul `saport`.

# Używanie
Wsystkie solvery które implemenowaliśmy znajdują się w folderze `TEMPLATES/{solver_name}/solver.py`, gdzie `solver_name`, to jedno z:
- integer
- assigment 
- knapsack
- maxflow
- minimax
- simplex
- critical_path
- rakinkgs

W każdym solverze jest podany przykładowy problem, który można zmodyfikować i dostosować do swoich potrzeb.
