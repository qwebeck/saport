from saport.assignment.model import AssignmentProblem
from saport.assignment.hungarian_solver import Solver
import os


def run():
    # Describe your problem in file problem.txt
    # First line is
    # <objective type> <n_workers> <n_tasks>
    dirname = os.path.dirname(os.path.realpath(__file__))
    problem_path = "zad_4.txt"
    model = AssignmentProblem.from_file(
        os.path.join(dirname, problem_path))
    solver = Solver(model)
    solution = solver.solve()
    print(solution)
    """
    Odpowiedź
    Assignment(assigned_tasks=[2, 1, 3, 4, 0], objective=34)
    Biorąc pod uwagę to, jak zapisałem macierz: 
        wiersz - rejony, kolumny - pracownicy
    
    Traktujemy to tak:
     Do rejonu R1 (ten co pod indeksem 0) jest przypisany samochód S3 (ponieważ ma indeks 2)
     Do rejonu R2 - S2
     .....
    
    Maksymalna ilość klientów którą możemy obsłużyć wynosi 34
    """


if __name__ == '__main__':
    run()
