from saport.simplex.model import Model


def run():
    model = Model("Zadanie 1")
    w1 = model.create_variable("W1")
    w2 = model.create_variable("W2")
    w3 = model.create_variable("W3")
    w4 = model.create_variable("W4")

    model.add_constraint(w1 + 3*w2 + w3 + w4 <= 600)
    model.add_constraint(4*w1 + w2 + w3 + 5*w4 <= 1200)
    model.maximize(8*w1 + 9*w2 + 6*w3 + 10*w4)

    try:
        solution = model.solve()
        print(solution)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    run()
