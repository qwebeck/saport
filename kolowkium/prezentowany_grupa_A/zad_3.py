from saport.simplex.model import Model


def run():
    model = Model("Zadanie 2")
    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(2*x1 - 5*x2 + x3 >= 14)
    model.add_constraint(x1 + 2*x3 >= 14)
    model.add_constraint(4*x1 + 5*x2 - 2*x3 == 18)

    model.maximize(4*x1 + 3*x2 + 5*x3)

    model.print_initial_state()
    try:
        solution = model.solve()
        print(solution)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    run()
