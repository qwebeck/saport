from setuptools import setup

setup(
    name='saport',
    description="Student's Attempt to Produce an Operation Research Toolkit",
    install_requires=[
        "decorator",
        "networkx",
        "numpy",
    ]
)
